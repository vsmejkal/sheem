# Sheem: Sheet Music Reader #

Sheem is a sheet music reader for Android that follows you whilst you play the piano. The application listens to you and automatically scrolls the sheet music position so that you can still see the upcoming measures. No additional pedals or equipment needed - just your Android phone or tablet.

### Features ###

- 26 selected music scores (traditional, classical, popular)
- Adapts to your own tempo - from very slow to fast
- No calibration needed

### Download ###

- Download from [Play Store](https://play.google.com/store/apps/details?id=cz.vsmejkal.sheem)

