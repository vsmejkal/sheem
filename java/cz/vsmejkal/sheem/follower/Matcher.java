package cz.vsmejkal.sheem.follower;

import android.os.ConditionVariable;
import android.util.Log;

public class Matcher {
    private static final String TAG = "Matcher";

    // DTW window size
    private static final int WIDTH = 100;
    // DTW window hop ratio
    private static final double HOP = 0.7;
    // Length of slope history
    private static final int SLOPES_LEN = 20;
    // Length of tempo history
    private static final int TEMPOS_LEN = 50;
    // Local search range for slope
    private static final int SLOPE_R = 5;
    // Max stddev for stable path
    private static final double STABILITY = 0.985;

    private FeatureSet mScorefts;
    private FeatureSet mAudiofts;

    private ConditionVariable mFrameReady =  new ConditionVariable(false);
    private ConditionVariable mPosReady = new ConditionVariable(false);

    private Thread mDTW;
    private float mPos = 0;
    private float mTempo = 1.0f;
    private boolean mStable = false;
    private volatile boolean mRunning = false;


    public Matcher(FeatureSet scoreFeatures, int start) {
        start = Math.min(Math.max(0, start), scoreFeatures.size()-1);

        mScorefts = new FeatureSet(scoreFeatures);
        mAudiofts = new FeatureSet(WIDTH + 10);
        mScorefts.setOffset(start);

        mPos = start;
        mRunning = true;
        mDTW = new Thread(new Runnable() {
            @Override
            public void run() { loop(); }
        });
        mDTW.start();
    }
    
    public void finish() {
        mRunning = false;
        mFrameReady.open();
        mPosReady.open();

        if (mDTW != null && mDTW.isAlive()) {
            try {
                mDTW.join();
            }
            catch (InterruptedException e) {
                Log.i(TAG, "InterruptedException");
            }
        }
    }

    public int findPos(FeatureVector audioft) {
        mAudiofts.add(audioft);

        if (mRunning) {
            mFrameReady.open();
            waitFor(mPosReady);
        }

        return (int) Math.min(mPos, mScorefts.size()-1);
    }

    public boolean isRunning() {
        return mRunning;
    }

    private boolean waitFor(ConditionVariable cond) {
        cond.block();
        cond.close();
        return mRunning;
    }

    private void loop() {
        float[][] cost = new float[WIDTH][WIDTH];
        int t = 0;
        int j = 0;
        int offset = (int)mPos;
        int winsize = Math.min(mScorefts.sizeFromOffset(), WIDTH);

        History slopes = new History(SLOPES_LEN);
        History slopes_w = new History(SLOPES_LEN);
        History tempos = new History(TEMPOS_LEN);

        if (!waitFor(mFrameReady))
            return;
        mPosReady.open();

        while (mScorefts.sizeFromOffset() > 2) {
            fillColumn(t, winsize, cost);

            while (t < winsize - 1 && j < winsize - 1) {
                if (!waitFor(mFrameReady))
                    return;
                t += 1;
                fillColumn(t, winsize, cost);

                // Update tempo
                float slope = getSlope(t, winsize, cost);
                slopes.add(slope * t);
                slopes_w.add(t);
                mTempo = slopes.sum() / slopes_w.sum();

                // Update score position
                j = Math.min(Math.round(t * mTempo), winsize - 1);
                mPos = offset + j;
                mPosReady.open();

                // Update path stability
                // TODO: enable stability
//                if (!mStable) {
//                    tempos.add(mTempo);
//                    mStable = isPathStable(tempos);
//                }

//                Log.i(TAG, String.format("Tempo: %f", mTempo));
            }

            // Prevent recursion
            int fx, fy;
            if (mScorefts.sizeFromOffset() >= 20) {
                fx = (int)(HOP * t);
                fy = (int)(HOP * j);
            }
            else {
                fx = t;
                fy = j;
            }

            // Remove old audio features
            mAudiofts.removeFirst(fx);

            // Move the score features offset
            mScorefts.setOffset(mScorefts.getOffset() + fy);

            t -= fx;
            j -= fy;
            offset += fy;
            winsize = Math.min(mScorefts.sizeFromOffset(), WIDTH);

            for (int x = 0; x < t; x++)
                fillColumn(x, winsize, cost);
        }

        mRunning = false;
    }

    private float getSlope(int w, int winsize, float[][] cost) {
        int step = 2;
        int min_h = Math.max((int)(Conf.MIN_SLOPE * (w + 1)) - 1, 0);
        int max_h = Math.min((int)(Conf.MAX_SLOPE * (w + 1)), winsize) - 1;

        // Once the system is stable, allow only local deviations
        if (mStable) {
            step = 1;
            min_h = Math.min(max_h, Math.max(min_h, (int)(w * mTempo - SLOPE_R)));
            max_h = Math.min(max_h, Math.max(min_h, (int)(w * mTempo + SLOPE_R)));
        }

        float slope_cost = Float.MAX_VALUE;
        float slope_h = w;

        for (int h = min_h; h <= max_h; h += step) {
            final float val = pathCost(w, h, cost);
            if (val < slope_cost) {
                slope_cost = val;
                slope_h = h;
            }
        }
        return slope_h / w;
    }

    private float pathCost(int x, int y, float[][] cost) {
        return cost[y][x] / (float)Math.sqrt(x*x + y*y);
    }

    private boolean isPathStable(History tempos) {
        return tempos.isFull() && 1.0 - tempos.stddev() >= STABILITY;
    }

    private float distance(int x, int y) {
        return mAudiofts.get(x).distance(mScorefts.get(y));
    }

    private void fillColumn(int x, int stop, float[][] cost) {
        if (x == 0) {
            cost[0][x] = distance(x, 0);
            for (int y = 1; y < stop; y++)
                cost[y][x] = cost[y-1][x] + distance(x, y);
        }
        else {
            cost[0][x] = cost[0][x-1] + distance(x, 0);
            for (int y = 1; y < stop; y++) {
                float dist = distance(x, y);
                cost[y][x] = Math.min(Math.min(
                        cost[y-1][x] + dist,
                        cost[y][x-1] + dist),
                        cost[y-1][x-1] + 1.1f * dist);
            }
        }
    }
}
