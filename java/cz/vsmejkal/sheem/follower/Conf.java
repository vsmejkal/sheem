package cz.vsmejkal.sheem.follower;


public class Conf {
    public static final int SAMPLERATE = 11025;
    public static final int WINSIZE = 1024;      // 92ms
    public static final int HOPSIZE = 512;      // 46ms
    public static final int FFTSIZE = 12;

    public static final double MIN_SLOPE = 0.1;
    public static final double MAX_SLOPE = 2.5;

    public static final int MIDI_LOW = 24;
    public static final int MIDI_HIGH = 108;

    public static final String TEMPLATES_FILE = "templates.bin";
}
