package cz.vsmejkal.sheem.follower;


import android.content.Context;
import cz.vsmejkal.sheem.midi.MidiFile;
import cz.vsmejkal.sheem.midi.MidiNote;
import cz.vsmejkal.sheem.midi.MidiTrack;
import cz.vsmejkal.sheem.notation.TimeSignature;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class Feature {
    private static final String TAG = "Feature";

    // History for adaptive onset normalization
    private static final int HIST_LEN = 100;

    private History mHistory = new History(HIST_LEN);
    private float[] mPrevChroma = new float[Conf.FFTSIZE];
    private float   mPrevEnergy;

    // Fourier transform
    private static FFT mFFT = null;

    // Note templates
    private static float[][] mTemplates = null;

    // Frequency ranges for chroma extraction
    private static float[] mFreqRanges = null;

    // Auxiliary vectors
    private float[] mChroma = FeatureVector.newChroma();

    public void clearHistory() {
        Arrays.fill(mPrevChroma, FeatureVector.ZERO);
        mPrevEnergy = 0;
        mHistory.clear();
    }

    public static void initStaticData(Context ctx) {
        if (mFFT == null)
            mFFT = new FFT(Conf.WINSIZE);

        if (mFreqRanges == null)
            Feature.computeFrequencyRanges();

        if (mTemplates == null)
            Feature.loadNoteTemplates(ctx);
    }

    private static void computeFrequencyRanges() {
        final int size = Conf.MIDI_HIGH - Conf.MIDI_LOW + 2;
        mFreqRanges = new float[size];

        for (int pitch = Conf.MIDI_LOW; pitch <= Conf.MIDI_HIGH + 1; pitch++) {
            final float pow = (float) Math.pow(2.0, ((pitch - 0.5) - 69) / 12.0);

            mFreqRanges[pitch - Conf.MIDI_LOW] = pow * 440.0f * Conf.WINSIZE / Conf.SAMPLERATE;
        }
    }

    private static void loadNoteTemplates(Context ctx) {
        final DataInputStream data;
        final int size = Conf.MIDI_HIGH - Conf.MIDI_LOW + 1;
        mTemplates = new float[size][Conf.FFTSIZE];

        try {
            final InputStream istream = ctx.getAssets().open(Conf.TEMPLATES_FILE);
            data = new DataInputStream(new BufferedInputStream(istream));
        }
        catch (IOException e) {
            Log.e(TAG, "Could not open templates file: " + Conf.TEMPLATES_FILE);
            throw new RuntimeException(e.getMessage());
        }

        try {
            for (int i = 0; i <= Conf.MIDI_HIGH - Conf.MIDI_LOW; i++)
                for (int j = 0; j < Conf.FFTSIZE; j++)
                    mTemplates[i][j] = data.readFloat();
        }
        catch (IOException e) {
            Log.e(TAG, "IOError while reading " + Conf.TEMPLATES_FILE);
            throw new RuntimeException(e.getMessage());
        }
    }

    private ArrayList<ArrayList<MidiNote>> buildMidiIndex(MidiFile midi) {
        final TimeSignature ts = midi.getTime();
        final double pulseToSec = ts.getTempo() * 1e-6 / ts.getQuarter();
        final double duration = midi.getTotalPulses() * pulseToSec;
        final double timeStep = Conf.HOPSIZE / (Conf.SAMPLERATE * pulseToSec);
        final int size = (int)(duration * Conf.SAMPLERATE / (float)Conf.HOPSIZE) + 1;

        ArrayList<ArrayList<MidiNote>> index = new ArrayList<ArrayList<MidiNote>>(size);
        for (int t = 0; t < size; t++)
            index.add(t, new ArrayList<MidiNote>(10));

        for (MidiTrack track: midi.getTracks()) {
            for (MidiNote note: track.getNotes()) {
                int start = (int)(note.getStartTime() / timeStep);
                int end = (int)(note.getEndTime() / timeStep);
                for (int t = start; t <= end && t < size; t++)
                    index.get(t).add(note);
            }
        }
        return index;
    }

    private float[] noteTemplate(double time, double pulseToSec, ArrayList<MidiNote> notes) {
        final double decay = 0.1;
        final double sustain = 0.1;
        final double release = 20;
        final double w_base = 64 * 64 * 64 * 64;
        Arrays.fill(mChroma, FeatureVector.ZERO);

        for (MidiNote note: notes) {
            double d_s = time - pulseToSec * note.getStartTime();
            double d_e = time - pulseToSec * note.getEndTime();
            double w_ds = (d_s < 0) ? 0 : Math.max(Math.pow(decay, d_s), sustain);
            double w_r = (d_e < 0) ? 1 : Math.max(1 - release * d_e, 0);
            double weight = w_ds * w_r * w_base;

            final float[] tp = mTemplates[note.getNumber() - Conf.MIDI_LOW];
            for (int i = 0; i < Conf.FFTSIZE; i++)
                mChroma[i] += weight * tp[i];
        }

        return mChroma;
    }

    public FeatureSet fromMidi(MidiFile midi) {
        ArrayList<ArrayList<MidiNote>> index = buildMidiIndex(midi);
        FeatureSet features = new FeatureSet(index.size());

        final TimeSignature ts = midi.getTime();
        final double pulseToSec = ts.getTempo() * 1e-6 / ts.getQuarter();
        final double timeStep = 1.0 / (double)Conf.SAMPLERATE;
        clearHistory();

        for (int t = 0; t < index.size(); t++) {
            double time = (t * Conf.HOPSIZE + Conf.WINSIZE / 2) * timeStep;
            float[] chroma = noteTemplate(time, pulseToSec, index.get(t));

            FeatureVector vec = new FeatureVector();
            vec.setOnset(getOnset(chroma));
            vec.setChroma(chroma);
            features.add(vec);
        }

        clearHistory();
        return features;
    }

    // Accumulate chroma vector from audio frame
    private float[] getChroma(final short[] samples) {
        float[] bands = mFFT.executeWithAudioBuffer(samples);
        Arrays.fill(mChroma, FeatureVector.ZERO);

        for (int p = Conf.MIDI_LOW; p <= Conf.MIDI_HIGH; p++) {
            final float flow = mFreqRanges[p - Conf.MIDI_LOW];
            final float fhigh = mFreqRanges[p - Conf.MIDI_LOW + 1];
            final int iflow = (int)flow;
            final int ifhigh = (int)fhigh;
            final int c = p % 12;

            if (iflow == ifhigh) {
                mChroma[c] += bands[iflow] * (fhigh - flow);
            }
            else {
                mChroma[c] += bands[iflow] * (1.0 - (flow - iflow));
                mChroma[c] += bands[ifhigh] * (fhigh - ifhigh);

                for (int f = iflow + 1; f < ifhigh; f++)
                    mChroma[c] += bands[f];
            }
        }

        return mChroma;
    }

    // Compute energy difference with adaptive normalization
    private float getOnset(float[] chroma) {
        float energy = 0;
        for (int i = 0; i < chroma.length; i++)
            energy += Math.max(0.0f, chroma[i] - mPrevChroma[i]);

        float onset = (float) Math.log(Math.max(1.0f, energy - mPrevEnergy));

        System.arraycopy(chroma, 0, mPrevChroma, 0, chroma.length);
        mPrevEnergy = energy;

        if (onset > 3) {
            mHistory.add(onset);
            onset /= mHistory.max();
            onset *= onset;
        }
        else {
            onset = 0;
        }

        return onset;
    }

    public FeatureVector fromAudio(final short[] samples) {
        FeatureVector vec = new FeatureVector();

        float[] chroma = getChroma(samples);
        float onset = getOnset(chroma);
        vec.setChroma(chroma);
        vec.setOnset(onset);

        return vec;
    }
}

