package cz.vsmejkal.sheem.follower;

import android.app.AlertDialog;
import android.content.DialogInterface;

import cz.vsmejkal.sheem.R;
import cz.vsmejkal.sheem.midi.MidiFile;
import cz.vsmejkal.sheem.midi.MidiOptions;
import cz.vsmejkal.sheem.notation.SheetMusic;
import cz.vsmejkal.sheem.ui.SheetMusicActivity;


public class ScoreFollower {
    private static final String TAG = "ScoreFollower";

    private Feature mFeature;
    private Matcher mMatcher;
    private AudioRecorder mRecorder;
    private FeatureSet mScoreFeatures;
    private boolean mRunning;
    private int mPos;
    private SheetMusic mReader;
    private SheetMusicActivity mActivity;

    public ScoreFollower(SheetMusicActivity activity) {
        mFeature = new Feature();
        mRunning = false;
        mPos = 0;
        mActivity = activity;
    }

    public void loadScore(MidiFile file, MidiOptions opt) {
        if (mRunning)
            stop();
        mScoreFeatures = mFeature.fromMidi(file);
        mPos = 0;
    }

    public void setReader(SheetMusic reader) {
        mReader = reader;
    }

    public void start() {
        if (mRunning)
            return;

        mFeature.clearHistory();
        mMatcher = new Matcher(mScoreFeatures, mPos);
        mRecorder = new AudioRecorder() {
            @Override
            public void onSamplesReady(short[] buffer) {
                FeatureVector vec = mFeature.fromAudio(buffer);

                if (mRunning && !mMatcher.isRunning()) {
                    mActivity.stop();
                }

                if (mRunning && vec.isMusic()) {
                    mPos = mMatcher.findPos(vec);
                    double time = mPos * Conf.HOPSIZE / (double)Conf.SAMPLERATE;

                    if (mReader != null)
                        mReader.setCursorPosition(time);
                }
            }

            @Override
            public void onError() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
                        dialog.setIcon(android.R.drawable.ic_dialog_alert);
                        dialog.setTitle(mActivity.getResources().getString(R.string.unsupported_mic_err_title));
                        dialog.setMessage(mActivity.getResources().getString(R.string.unsupported_mic_err));
                        dialog.setNeutralButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                        dialog.show();
                    }
                });
            }
        };
        mRecorder.start();
        mRunning = true;
    }

    public synchronized void stop() {
        mRunning = false;
        mRecorder.finish();
        mMatcher.finish();
    }

    public double getPosition() {
        return mPos * Conf.HOPSIZE / (double)Conf.SAMPLERATE;
    }

    public void setPosition(double time) {
        int pos = (int)(time * Conf.SAMPLERATE / (double)Conf.HOPSIZE);
        boolean wasRunning = mRunning;

        mRunning = false;
        mMatcher.finish();
        mMatcher = new Matcher(mScoreFeatures, pos);
        mPos = pos;
        mRunning = wasRunning;
    }

    public boolean isRunning() {
        return mRunning;
    }

}
