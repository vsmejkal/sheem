package cz.vsmejkal.sheem.follower;


public class History {
    private final float[] mBuffer;
    private final int mCapacity;
    private int mHead = 0;
    private int mSize = 0;
    private boolean mIsFull = false;

    public History(final int capacity) {
        if (capacity <= 0)
            throw new IllegalArgumentException("Capacity must be greater than zero");
        mBuffer = new float[capacity];
        mCapacity = capacity;
    }

    public void add(final float elm) {
        mBuffer[mHead] = elm;
        mHead = (mHead + 1) % mCapacity;

        if (!mIsFull) {
            mSize += 1;
            mIsFull = mSize == mCapacity;
        }
    }

    public float get(final int i) {
        return mBuffer[i];
    }

    public float sum() {
        float sum = 0;
        for (int i = 0; i < mSize; i++)
            sum += mBuffer[i];
        return sum;
    }

    public float max() {
        float max = Float.NEGATIVE_INFINITY;
        for (int i = 0; i < mSize; i++)
            if (mBuffer[i] > max)
                max = mBuffer[i];
        return max;
    }

    public float avg() {
        return sum() / mSize;
    }

    public float stddev() {
        float mean = sum() / mSize;
        float temp = 0;
        for (int i = 0; i < mSize; i++)
            temp += (mean - mBuffer[i]) * (mean - mBuffer[i]);

        return (float) Math.sqrt(temp / mSize);
    }

    public boolean isFull() {
        return mIsFull;
    }

    public void clear() {
        mHead = 0;
        mSize = 0;
        mIsFull = false;
    }
}

