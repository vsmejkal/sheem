package cz.vsmejkal.sheem.follower;

/**
 * Fast Fourier Transform
 *
 * See: http://stackoverflow.com/questions/9272232/fft-library-in-android-sdk
 */
public class FFT {

    int n, m, size;

    // Lookup tables. Only need to recompute when size of FFT changes.
    private final double[] cos;
    private final double[] sin;

    // FFT input and output arrays
    private double[] win;
    private double[] in;
    private double[] out;
    private float[] bands;

    public FFT(int n) {
        this.n = n;
        this.m = (int) (Math.log(n) / Math.log(2));
        this.size = n;

        // Make sure n is a power of 2
        if (n != (1 << m))
            throw new RuntimeException("FFT length must be power of 2");

        win = new double[n];
        in = new double[n];
        out = new double[n];
        bands = new float[n / 2];

        // Precompute tables
        cos = new double[n / 2];
        sin = new double[n / 2];

        for (int i = 0; i < size / 2; i++) {
            cos[i] = Math.cos(-2 * Math.PI * i / n);
            sin[i] = Math.sin(-2 * Math.PI * i / n);
        }

        for (int i = 0; i < size; i++) {
            // Hann window
            win[i] = 0.5 * (1.0 - Math.cos(2.0 * Math.PI * i / (size - 1.0))) / 65536.0;

            // Blackman window
            /*win[i] = 0.42 - 0.50 * Math.cos(2 * Math.PI * i / (size-1))
                          + 0.08 * Math.cos(4 * Math.PI * i / (size-1));*/
        }
    }

    public float[] executeWithAudioBuffer(short[] samples) {
        for (int i = 0; i < n; ++i) {
            in[i] = samples[i] * win[i];
            out[i] = 0;
        }

        fft(in, out);

        final int bandsSize = n / 2;
        for (int i = 0; i < bandsSize; ++i) {
            bands[i] = (float) Math.sqrt(in[i]*in[i] + out[i]*out[i]);
        }

        return bands;
    }

    /***************************************************************
     * fft.c
     * Douglas L. Jones
     * University of Illinois at Urbana-Champaign
     * January 19, 1992
     * http://cnx.rice.edu/content/m12016/latest/
     *
     *   fft: in-place radix-2 DIT DFT of a complex input
     *
     *   input:
     * n: length of FFT: must be a power of two
     * m: n = 2**m
     *   input/output
     * x: double array of length n with real part of data
     * y: double array of length n with imag part of data
     *
     *   Permission to copy and use this program is granted
     *   as long as this header is included.
     ****************************************************************/
    public void fft(double[] x, double[] y) {
        int i, j, k, n1, n2, a;
        double c, s, t1, t2;

        // Bit-reverse
        j = 0;
        n2 = n / 2;
        for (i = 1; i < n - 1; i++) {
            n1 = n2;
            while (j >= n1) {
                j = j - n1;
                n1 = n1 / 2;
            }
            j = j + n1;

            if (i < j) {
                t1 = x[i];
                x[i] = x[j];
                x[j] = t1;
                t1 = y[i];
                y[i] = y[j];
                y[j] = t1;
            }
        }

        // FFT
        n1 = 0;
        n2 = 1;

        for (i = 0; i < m; i++) {
            n1 = n2;
            n2 = n2 + n2;
            a = 0;

            for (j = 0; j < n1; j++) {
                c = cos[a];
                s = sin[a];
                a += 1 << (m - i - 1);

                for (k = j; k < n; k = k + n2) {
                    t1 = c * x[k + n1] - s * y[k + n1];
                    t2 = s * x[k + n1] + c * y[k + n1];
                    x[k + n1] = x[k] - t1;
                    y[k + n1] = y[k] - t2;
                    x[k] = x[k] + t1;
                    y[k] = y[k] + t2;
                }
            }
        }
    }
}
