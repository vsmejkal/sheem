package cz.vsmejkal.sheem.follower;

import java.util.ArrayList;


class MyArrayList<E> extends ArrayList<E> {
    public MyArrayList(int capacity) {
        super(capacity);
    }

    @Override
    public void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
    }
}


public class FeatureSet {
    private MyArrayList<FeatureVector> mBuffer;
    private int mOffset = 0;

    public FeatureSet(int capacity) {
        mBuffer = new MyArrayList<FeatureVector>(capacity);
    }

    public FeatureSet(FeatureSet fts) {
        mBuffer = fts.getBuffer();
    }

    public MyArrayList<FeatureVector> getBuffer() {
        return mBuffer;
    }

    public int getOffset() {
        return mOffset;
    }

    public void setOffset(int start) {
        int size = mBuffer.size();
        if (start >= size)
            throw new IndexOutOfBoundsException("Invalid start " + start + ", size is " + size);
        mOffset = start;
    }

    public FeatureVector get(int index) {
        return mBuffer.get(mOffset + index);
    }

    public void add(FeatureVector vec) {
        mBuffer.add(vec);
    }

    public int size() {
        return mBuffer.size();
    }

    public int sizeFromOffset() {
        return mBuffer.size() - mOffset;
    }

    public void removeFirst(int toIndex) {
        mBuffer.removeRange(0, toIndex);
        mOffset = Math.max(0, mOffset - toIndex);
    }
}


