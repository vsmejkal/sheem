package cz.vsmejkal.sheem.follower;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;


abstract public class AudioRecorder extends Thread {
    private static final String TAG = "AudioRecorder";

    private static final int AUDIO_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;

    private int mSampleRate;
    private int mWinSize;
    private int mHopSize;
    private int mAudioScale;

    private volatile AudioRecord mAudioRecord;
    private volatile boolean mStopped;

    // Buffer for mic data, can be upsampled
    private short[] mRecBuffer;

    // Output buffer with correctly sampled data
    private short[] mOutBuffer;


    public boolean initialize() {
        final int rate = findValidSampleRate();
        if (rate == 0)
            return false;

        mAudioScale = rate / Conf.SAMPLERATE;
        mSampleRate = mAudioScale * Conf.SAMPLERATE;
        mWinSize = mAudioScale * Conf.WINSIZE;
        mHopSize = mAudioScale * Conf.HOPSIZE;
        mStopped = false;

        mRecBuffer = new short[mWinSize];
        mOutBuffer = new short[Conf.WINSIZE];

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        return true;
    }

    @Override
    public void run() {
        if (mStopped)
            return;

        if (!initialize()) {
            onError();
            return;
        }

        final int minBufferSize = Math.max(2 * mWinSize,
                AudioRecord.getMinBufferSize(mSampleRate, AUDIO_CHANNELS, AUDIO_FORMAT));

        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                mSampleRate, AUDIO_CHANNELS, AUDIO_FORMAT, minBufferSize);
        mAudioRecord.startRecording();

        recordingLoop();
    }

    public void finish() {
        stopRecording();
    }

    private void recordingLoop() {
        int offset = 0;

        while (!mStopped) {
            try {
                offset += mAudioRecord.read(mRecBuffer, offset, mWinSize - offset);
            }
            catch (Throwable t) {
                Log.e(TAG, "AudioRecord read has failed");
                t.printStackTrace();
            }

            if (!mStopped && offset == mWinSize) {
                if (mAudioScale == 1) {
                    onSamplesReady(mRecBuffer);
                }
                else {
                    downsample(mRecBuffer, mOutBuffer, mAudioScale);
                    onSamplesReady(mOutBuffer);
                }
                shiftLeft(mRecBuffer, mHopSize);
                offset -= mHopSize;
            }
        }

        stopRecording();
    }

    private void stopRecording() {
        if (!mStopped && mAudioRecord != null) {
            mStopped = true;
            interrupt();
            mAudioRecord.stop();
            mAudioRecord.release();
            mAudioRecord = null;
        }
    }

    private void downsample(short[] input, short[] output, int scale) {
        for (int i = 0, j = 0; j < output.length; i += scale, j++)
            output[j] = input[i];
    }

    private void shiftLeft(short[] buffer, int shift) {
        for (int i = 0, j = shift; j < buffer.length; i++, j++)
            buffer[i] = buffer[j];
    }

    private int findValidSampleRate() {
        for (int rate: new int[] {11025, 22050, 44100}) {
            int bufferSize = AudioRecord.getMinBufferSize(rate, AUDIO_CHANNELS, AUDIO_FORMAT);
            if (bufferSize > 0)
                return rate;
        }
        return 0;
    }

    abstract public void onSamplesReady(short[] buffer);

    abstract public void onError();
}
