package cz.vsmejkal.sheem.follower;

import java.util.Arrays;


public class FeatureVector {
    // Value to initialize chroma vector
    public static final float ZERO = 1e-16f;

    // Min chroma std. deviation for musical frame
    private static final double MUSIC_FRAME = 0.7;

    public byte[] chroma;
    public float onset;
    private float norm;

    FeatureVector() {
        this.chroma = new byte[Conf.FFTSIZE];
        this.onset = 0;
        this.norm = 1;
    }

    public static float[] newChroma() {
        float[] vec = new float[Conf.FFTSIZE];
        Arrays.fill(vec, FeatureVector.ZERO);
        return vec;
    }

    public boolean isMusic() {
        int sum = 0;
        for (int c: chroma)
            sum += c;

        float mean = sum / 12f;
        float temp = 0;
        for (int c: chroma)
            temp += (mean - c) * (mean - c);

        double stddev = Math.sqrt(temp / 12f);

        return stddev >= MUSIC_FRAME;
    }

    public float distance(FeatureVector other) {
        float d_c = 0;
        float d_o = Math.abs(this.onset - other.onset);
        float norm = this.norm + other.norm;

        for (int i = 0; i < Conf.FFTSIZE; i++)
            d_c += Math.abs(this.chroma[i] - other.chroma[i]);

        return (3 * d_c / norm + d_o);
    }

    private void normalizeChroma(float[] in, float[] out) {
        double norm = 0;
        for (float x: in)
            norm += x;

        if (norm > 0)
            for (int i = 0; i < Conf.FFTSIZE; i++)
                out[i] = (float)(in[i] / norm);
    }

    private void quantizeChroma(float[] in, byte[] out) {
        for (int i = 0; i < Conf.FFTSIZE; i++) {
            if (in[i] < 0.05)
                out[i] = 0;
            else if (in[i] < 0.1)
                out[i] = 1;
            else if (in[i] < 0.2)
                out[i] = 2;
            else if (in[i] < 0.4)
                out[i] = 3;
            else
                out[i] = 4;
        }
    }

    private float quantizeOnset(float onset) {
        if (onset > 0.5)
            return 1.0f;
        else if (onset > 0)
            return 0.5f;
        else
            return 0.0f;
    }

    public void setChroma(float[] chroma) {
        normalizeChroma(chroma, chroma);
        quantizeChroma(chroma, this.chroma);

        this.norm = 0;
        for (byte x: this.chroma)
            this.norm += x;
    }

    public void setOnset(float onset) {
        this.onset = onset;//quantizeOnset(onset);
    }

    public String toString() {
        String output = "";
        for (int x: this.chroma)
            output += Integer.toString(x) + ", ";
        return "[" + output + "], onset=" + Float.toString(onset);
    }
}
