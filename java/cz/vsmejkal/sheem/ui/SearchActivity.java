package cz.vsmejkal.sheem.ui;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import cz.vsmejkal.sheem.R;
import cz.vsmejkal.sheem.library.ScoreArray;
import cz.vsmejkal.sheem.library.ScoreInfo;
import cz.vsmejkal.sheem.library.SearchEngine;
import cz.vsmejkal.sheem.library.SearchRequest;
import cz.vsmejkal.sheem.library.engines.MuseScore;

public class SearchActivity extends ActionBarActivity
                            implements ScoreListFragment.Listener, SearchEngine.Listener {

    private SearchEngine mSearchEngine;
    private ScoreListFragment mListFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        FragmentManager fm = getSupportFragmentManager();
        mListFragment = (ScoreListFragment) fm.findFragmentById(R.id.search_list);

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSearchEngine != null)
            mSearchEngine.stop();
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(query);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);

            searchForScore(query);
        }
    }

    private void searchForScore(final String query) {
        if (mSearchEngine == null)
            mSearchEngine = new MuseScore(this);

        SearchRequest req = new SearchRequest(query);
        mListFragment.setListShown(false);
        mSearchEngine.search(req);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.search_score:
                onSearchRequested();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFragmentItemClick(ScoreInfo score) {
        Log.i("Score clicked", score.title);
    }

    @Override
    public void onSearchFinished(final ScoreArray results) {
        Log.i("MuseScore", "search finished");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mListFragment.setData(results);
            }
        });
    }

    @Override
    public void onSearchError(int err) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        Resources res = getResources();
        if (err == R.string.connection_err) {
            dialog.setTitle(res.getString(R.string.connection_err_title));
            dialog.setMessage(res.getString(R.string.connection_err));
        }
        else {
            dialog.setTitle(res.getString(R.string.server_err_title));
            dialog.setMessage(res.getString(R.string.server_err));
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                mListFragment.setListShown(true);
            }
        });
    }
}