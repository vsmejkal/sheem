package cz.vsmejkal.sheem.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import cz.vsmejkal.sheem.R;
import cz.vsmejkal.sheem.follower.Feature;
import cz.vsmejkal.sheem.follower.ScoreFollower;
import cz.vsmejkal.sheem.midi.MidiFile;
import cz.vsmejkal.sheem.midi.MidiFileException;
import cz.vsmejkal.sheem.midi.MidiOptions;
import cz.vsmejkal.sheem.notation.FileUri;


public class SheetMusicActivity extends Activity {
    private static final String TAG = "SheetMusicActivity";

    private SystemUiHider mSystemUiHider;
    private ScoreFollower mFollower;
    private SheetMusicView mView;
    private String mScore;
    private boolean mPaused;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sheetmusic);
        setAutohiding();

        // Keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Load static data
        Feature.initStaticData(this);

        mView = (SheetMusicView) findViewById(R.id.sheetmusic_view);
        mFollower = new ScoreFollower(this);

        mView.getReader().setFollower(mFollower);
        mFollower.setReader(mView.getReader());

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(LibraryActivity.PATH)) {
            loadScore(intent.getStringExtra(LibraryActivity.PATH));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mScore != null) {
            outState.putString("score", mScore);
            outState.putDouble("position", mFollower.getPosition());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);

        if (savedState.containsKey("score"))
            loadScore(savedState.getString("score"));

        if (savedState.containsKey("position"))
            setPosition(savedState.getDouble("position"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPaused = true;
        stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPaused = false;
        start();
    }

    public void start() {
        if (!mPaused) {
            mView.getReader().setCursorVisible(true);
            mFollower.start();

            Button btn = (Button) findViewById(R.id.play_btn);
            btn.setText(cz.vsmejkal.sheem.R.string.stop_btn);
        }
    }

    public void stop() {
        mView.getReader().setCursorVisible(false);
        mFollower.stop();

        Button btn = (Button) findViewById(R.id.play_btn);
        btn.setText(cz.vsmejkal.sheem.R.string.play_btn);
    }

    public void setPosition(double time) {
        mView.getReader().setCursorPosition(time);
        mFollower.setPosition(time);
    }

    public void loadScore(final String path) {
        if (mScore != null && mScore.equals(path))
            return;

        FileUri file = new FileUri(Uri.parse(path), null);
        MidiFile midifile;
        mScore = path;

        try {
            byte[] data = file.getData(this);
            midifile = new MidiFile(data, file.toString());
        }
        catch (MidiFileException e) {
            Log.e(TAG, "MidiFileException: " + e.getMessage());
            throw e;
        }

        MidiOptions opt = new MidiOptions(midifile);
        opt.scrollVert = true;

        mView.getReader().readFile(midifile, opt);
        mFollower.loadScore(midifile, opt);
    }


    /**************************************************************************
     *                     UI AUTOHIDING HANDLERS
     *************************************************************************/

    // Whether or not the system UI should be auto-hidden after delay.
    private static final boolean AUTO_HIDE = true;

    // How long to wait before hiding the system UI.
    private static final int AUTO_HIDE_DELAY_MILLIS = 2000;

    // The flags to pass to {@link SystemUiHider#getInstance}.
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;


    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private void setAutohiding() {
        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.sheetmusic_view);
        controlsView.setVisibility(View.GONE);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                    /*    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }*/
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
//        contentView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mSystemUiHider.toggle();
//            }
//        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.play_btn).setOnTouchListener(mDelayHideTouchListener);
    }
}
