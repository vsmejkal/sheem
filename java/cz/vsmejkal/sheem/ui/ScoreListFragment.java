package cz.vsmejkal.sheem.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;

import cz.vsmejkal.sheem.R;
import cz.vsmejkal.sheem.library.ScoreArray;
import cz.vsmejkal.sheem.library.ScoreInfo;

/**
 * A fragment representing a list of Items.
 * <p />
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p />
 * Activities containing this fragment MUST implement the OnItemClickListener
 * interface.
 */
public class ScoreListFragment extends ListFragment implements AbsListView.OnItemClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Listener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;
    private ScoreArray mScores = new ScoreArray();
    private Adapter mAdapter;

    // TODO: Rename and change types of parameters
    public static ScoreListFragment newInstance(String param1, String param2) {
        ScoreListFragment fragment = new ScoreListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ScoreListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


//        mAdapter = new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
//                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);
        mAdapter = new Adapter(getActivity(), mScores.getArray());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_score, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (Listener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setListShown(boolean shown) {
//        LinearLayout progress = (LinearLayout) getView().findViewById(R.id.progressContainer);
//        FrameLayout list = (FrameLayout) getView().findViewById(R.id.listContainer);

        View view = getView();
        ViewSwitcher viewSwitcher = (ViewSwitcher) view.findViewById(R.id.viewSwitcher);
        View firstView = view.findViewById(R.id.listContainer);
        View secondView = view.findViewById(R.id.progressContainer);

        if (shown/* && viewSwitcher.getCurrentView() != firstView*/) {
            viewSwitcher.setDisplayedChild(0);
        }
        else/* if (!shown && viewSwitcher.getCurrentView() != secondView)*/ {
            viewSwitcher.setDisplayedChild(1);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i("Score clicked", Integer.toString(position));

        if (mListener != null) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
//            mListener.onFragmentItemClick(mAdapter.getItem(position));
        }
        else
            Log.i("Fragment:", "onFragmentItemClick (listener=null)");
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (mListener != null) {
            mListener.onFragmentItemClick(mAdapter.getItem(position));
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    public void setData(ScoreArray scores) {
        mScores.replaceAll(scores);
        mAdapter.notifyDataSetChanged();
        setListShown(true);
    }

    // Fragment interface
    public interface Listener {
        public void onFragmentItemClick(ScoreInfo score);
    }

    // List adapter
    public class Adapter extends ArrayAdapter<ScoreInfo> {
        private Context context;

        public Adapter(Context context, ArrayList<ScoreInfo> items) {
            super(context, android.R.layout.simple_list_item_2, items);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(android.R.layout.simple_list_item_2, null);
            }

            ScoreInfo item = getItem(position);
            if (item != null) {
                TextView titleView = (TextView) view.findViewById(android.R.id.text1);
                TextView composerView = (TextView) view.findViewById(android.R.id.text2);

                if (titleView != null)
                    titleView.setText(item.title);

                if (composerView != null)
                    composerView.setText(item.composer);
            }

            return view;
        }
    }
}
