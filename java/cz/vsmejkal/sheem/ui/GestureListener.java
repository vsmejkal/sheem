package cz.vsmejkal.sheem.ui;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Scroller;

public class GestureListener extends GestureDetector.SimpleOnGestureListener {
    private final String TAG = "GestureListener";

    private final double EDGE_RATIO = 0.3;

    public final Scroller mScroller;
    public final SheetMusicView mView;

    private int mWidth = 0;
    private int mHeight = 0;
    private int mMinX = 0;
    private int mMaxX = 0;
    private int mMinY = 0;
    private int mMaxY = 0;
    private boolean mScrollVert = true;

    public GestureListener(Scroller scroller, SheetMusicView view) {
        super();
        mScroller = scroller;
        mView = view;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float dx, float dy) {
        // Is scrolling
        if (mScroller.computeScrollOffset()) {
            if (!mScrollVert) {
                final int posX = mScroller.getFinalX();
                if (posX + dx < mMinX || posX + dx > mMaxX)
                    dx *= EDGE_RATIO;

                mScroller.setFinalX(posX + (int)dx);
            }
            else {
                final int posY = mScroller.getFinalY();
                if (posY + dy < mMinY || posY + dy > mMaxY)
                    dy *= EDGE_RATIO;

                mScroller.setFinalY(posY + (int)dy);
            }
        }

        // Start scrolling
        else {
            if (!mScrollVert) {
                final int posX = mScroller.getCurrX();
                mScroller.startScroll(posX, 0, (int)dx, 0);
            }
            else {
                final int posY = mScroller.getCurrY();
                mScroller.startScroll(0, posY, 0, (int)dy);
            }
        }
        return true;
    }

    public void onScrollEnd() {
        final int posX = mScroller.getCurrX();
        final int posY = mScroller.getCurrY();
        final int newPosX = Math.max(mMinX, Math.min(mMaxX, posX));
        final int newPosY = Math.max(mMinY, Math.min(mMaxY, posY));

        if (!mScrollVert && posX != newPosX)
            mScroller.fling(newPosX, posY, 0, 0, mMinX, mMaxX, 0, 0);

        else if (mScrollVert && posY != newPosY)
            mScroller.fling(posX, newPosY, 0, 0, 0, 0, mMinY, mMaxY);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float vX, float vY) {
        if (mScroller.computeScrollOffset()) {
            int posX = mScroller.getCurrX();
            int posY = mScroller.getCurrY();
            mScroller.fling(posX, posY, (int)-vX, (int)-vY, mMinX, mMaxX, mMinY, mMaxY);
        }
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        if (!mScroller.isFinished()) { // Is flinging
            mScroller.forceFinished(true); // Stop flinging on touch
        }
        return true;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        mView.onSingleTap(e);
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        mView.onLongPress(e);
    }

    public void setScrollLimits(int maxX, int maxY) {
        mMaxX = maxX;
        mMaxY = maxY;
    }

    public void setViewport(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    public void setScrollVertical(boolean scrollVertical) {
        mScrollVert = scrollVertical;
    }
}