package cz.vsmejkal.sheem.ui;

import android.content.Context;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import cz.vsmejkal.sheem.notation.SheetMusic;
import cz.vsmejkal.sheem.renderer.GLRenderer;
import cz.vsmejkal.sheem.renderer.GLSymbols;
import cz.vsmejkal.sheem.renderer.MultisampleConfigChooser;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Scroller;

import javax.microedition.khronos.opengles.GL10;



public class SheetMusicView extends GLSurfaceView {

    private final GLRenderer mRenderer;
    private final GestureListener mGestureListener;
    private final GestureDetector mGestureDetector;
    private final SheetMusic mReader;
    private final Scroller mScroller;

    public SheetMusicView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Set the gesture detector
        mScroller = new Scroller(context);
        mGestureListener = new GestureListener(mScroller, this);
        mGestureDetector = new GestureDetector(context, mGestureListener);

        // Create sheet music reader
        mReader = new SheetMusic(this, mGestureListener);

        // Create renderer for this view
        mRenderer = new GLRenderer(context, this) {
            @Override
            public void onInit(Context ctx) {
                GLSymbols.loadImages(ctx);
            }

            @Override
            public void onSurfaceChanged(GL10 unused, int width, int height) {
                super.onSurfaceChanged(unused, width, height);
                mReader.onSizeChanged(width, height);
                mGestureListener.setViewport(width, height);
            }

            @Override
            public void onDraw(float[] vpMatrix) {
                // If scroll is still running, request another redraw
                if (mScroller.computeScrollOffset()) {
                    requestRender();
                    mScrollX = mScroller.getCurrX();
                    mScrollY = mScroller.getCurrY();
                }
                mReader.draw(vpMatrix, mScrollX, mScrollY);
            }
        };

        setEGLContextClientVersion(2);
        setEGLConfigChooser(new MultisampleConfigChooser());
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public void onSingleTap(MotionEvent e) {
        Point pos = new Point();
        pos.x = (int)e.getX();
        pos.y = (int)e.getY();

        SheetMusicActivity activity = (SheetMusicActivity) getContext();
        if (activity != null) {
            double time = mReader.pointToTime(pos);
            activity.setPosition(time);
            activity.start();
        }
    }

    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        // Check the scroll bounds
        if (e.getAction() == MotionEvent.ACTION_UP) {
            mGestureListener.onScrollEnd();
        }
        else if (e.getAction() == MotionEvent.ACTION_DOWN) {
            SheetMusicActivity activity = (SheetMusicActivity) getContext();
            if (activity != null)
                activity.stop();
        }

        mGestureDetector.onTouchEvent(e);
        requestRender();
        return true;
    }

    public void scrollBy(int dx, int dy, int duration) {
        mScroller.forceFinished(true);
        mScroller.startScroll(mRenderer.mScrollX, mRenderer.mScrollY, dx, dy, duration);
    }

    public SheetMusic getReader() {
        return mReader;
    }
}


