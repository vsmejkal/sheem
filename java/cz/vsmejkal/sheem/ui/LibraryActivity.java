package cz.vsmejkal.sheem.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import cz.vsmejkal.sheem.R;
import cz.vsmejkal.sheem.library.ScoreInfo;

public class LibraryActivity extends ActionBarActivity
                            implements ScoreListFragment.Listener {
    private static final String TAG = "ScoresActivity";

    private static final String ASSETS_URI = "file:///android_asset/";
    private static final String SONGS_LIST = "songs.json";

    public static final int CHOOSE_SONG = 1;

    public static final String TITLE = "title";
    public static final String AUTHOR = "author";
    public static final String PATH = "path";

    private ArrayList<HashMap<String, String>> mSongs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

//        readSongs();

//        ListAdapter adapter = new SimpleAdapter(this, mSongs,
//                android.R.layout.simple_list_item_2,
//                new String[] { TITLE, AUTHOR },
//                new int[] { android.R.id.text1, android.R.id.text2 });
//        setListAdapter(adapter);
    }

//    @Override
//    public void onListItemClick(ListView l, View v, int position, long id) {
//        HashMap<String, String> item = mSongs.get(position);
//
//        Intent intent = new Intent(this, SheetMusicActivity.class);
//        intent.putExtra(PATH, ASSETS_URI + item.get(PATH));
//        startActivity(intent);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.scores_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.search_score:
                onSearchRequested();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void readSongs() {
        String data;
        try {
            InputStream is = getAssets().open(SONGS_LIST);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            data = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        mSongs = new ArrayList<HashMap<String, String>>();

        try {
            JSONArray arr = new JSONArray(data);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject item = arr.getJSONObject(i);
                HashMap<String, String> song = new HashMap<String, String>();
                song.put(TITLE, item.getString(TITLE));
                song.put(AUTHOR, item.getString(AUTHOR));
                song.put(PATH, item.getString(PATH));
                mSongs.add(song);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void writeSongs() {

    }

    @Override
    public void onFragmentItemClick(ScoreInfo score) {
        Log.i("Score clicked:", score.title);
    }
}
