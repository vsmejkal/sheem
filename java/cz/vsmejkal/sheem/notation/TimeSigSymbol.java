/*
 * Copyright (c) 2007-2011 Madhav Vaidyanathan
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package cz.vsmejkal.sheem.notation;

import android.graphics.Paint;
import cz.vsmejkal.sheem.renderer.GLCanvas;
import cz.vsmejkal.sheem.renderer.GLSymbols;


/** @class TimeSigSymbol
 * A TimeSigSymbol represents the time signature at the beginning
 * of the staff. We use pre-made symbols for the numbers, instead of
 * drawing strings.
 */

public class TimeSigSymbol implements MusicSymbol {
    /** The symbols for each number */
    private static int[] symbols = new int[] {
            -1,
            -1,
            GLSymbols.TWO,
            GLSymbols.THREE,
            GLSymbols.FOUR,
            -1,
            GLSymbols.SIX,
            -1,
            GLSymbols.EIGHT,
            GLSymbols.NINE,
            -1,
            -1,
            GLSymbols.TWELVE
    };
    private int  numerator;         /** The numerator */
    private int  denominator;       /** The denominator */
    private int  width;             /** The width in pixels */
    private boolean candraw;        /** True if we can draw the time signature */

    /** Create a new TimeSigSymbol */
    public TimeSigSymbol(int numer, int denom) {
        numerator = numer;
        denominator = denom;
        if (numer >= 0 && numer < symbols.length && symbols[numer] != -1 &&
            denom >= 0 && denom < symbols.length && symbols[denom] != -1) {
            candraw = true;
        }
        else {
            candraw = false;
        }
        width = getMinWidth();
    }

    /** Get the time (in pulses) this symbol occurs at. */
    public int getStartTime() { return -1; }

    /** Get the minimum width (in pixels) needed to draw this symbol */
    public int getMinWidth() {
        if (candraw) 
            return (int)(2 * SheetMusic.NoteHeight * GLSymbols.aspectRatio(symbols[3]));
        else
            return 0;
    }

    /** Get/Set the width (in pixels) of this symbol. The width is set
     * in SheetMusic.AlignSymbols() to vertically align symbols.
     */
    public int getWidth()   { return width; }
    public void setWidth(int value) { width = value; }

    /** Get the number of pixels this symbol extends above the staff. Used
     *  to determine the minimum height needed for the staff (Staff.FindBounds).
     */
    public int getAboveStaff() {  return 0; }

    /** Get the number of pixels this symbol extends below the staff. Used
     *  to determine the minimum height needed for the staff (Staff.FindBounds).
     */
    public int getBelowStaff() { return 0; } 

    /** Draw the symbol.
     * @param ytop The ylocation (in pixels) where the top of the staff starts.
     */
    public 
    void Draw(GLCanvas canvas, Paint paint, int ytop) {
        if (!candraw)
            return;

        canvas.translate(getWidth() - getMinWidth(), 0);
        int numer = symbols[numerator];
        int denom = symbols[denominator];

        /* Scale the image width to match the height */
        float imgheight = SheetMusic.NoteHeight * 1.5f;
        float imgwidth = imgheight * GLSymbols.aspectRatio(numer);
        float y = ytop + SheetMusic.NoteHeight * 0.2f;
        canvas.drawSymbol(0, y, imgwidth, y + imgheight, numer);

        imgwidth = imgheight * GLSymbols.aspectRatio(denom);
        y = y + SheetMusic.NoteHeight * 2;
        canvas.drawSymbol(0, y, imgwidth, y + imgheight, denom);
        canvas.translate(-(getWidth() - getMinWidth()), 0);
    }

    public String toString() {
        return String.format("TimeSigSymbol numerator=%1$s denominator=%2$s",
                             numerator, denominator);
    }
}


