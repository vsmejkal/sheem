package cz.vsmejkal.sheem.renderer;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public abstract class GLObjects {

    protected final String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
            "attribute vec3 vPosition;" +
            "void main() {" +
            "  gl_Position = uMVPMatrix * vec4(vPosition, 1.0);" + "}";

    protected final String fragmentShaderCode =
            "precision mediump float;" +
            "uniform vec4 vColor;" +
            "void main() {" +
            "  gl_FragColor = vColor;" + "}";

    // Main buffers
    protected FloatBuffer mVertexBuffer;
    protected ShortBuffer mDrawListBuffer;

    // Helper buffers used for filling main buffers
    protected float[] mFloatBuffer;
    protected short[] mShortBuffer;

    static final int COORDS_PER_VERTEX = 2;
    static final int SIZEOF_FLOAT = 4;
    static final int SIZEOF_SHORT = 2;

    /* Size of object's vertex data */
    protected int mVertexSize;
    /* Size of object's drawlist item */
    protected int mDrawListSize;

    protected float[] mColor = {0f, 0f, 0f, 1f};
    protected int mCount = 0;
    protected int mCapacity = 64;
    protected int mProgram;

    protected void initialize() {
        mFloatBuffer = new float[mVertexSize];
        mShortBuffer = new short[mDrawListSize];

        mVertexBuffer = ByteBuffer
                .allocateDirect(mCapacity * mVertexSize * SIZEOF_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();

        if (mDrawListSize > 0) {
            mDrawListBuffer = ByteBuffer
                    .allocateDirect(mCapacity * mDrawListSize * SIZEOF_SHORT)
                    .order(ByteOrder.nativeOrder())
                    .asShortBuffer();
        }

        // Prepare shaders and OpenGL program
        final int vertexShader = GLHelper.loadShader(
                GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        final int fragmentShader = GLHelper.loadShader(
                GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLHelper.createAndLinkProgram(vertexShader, fragmentShader, new String[]{"vPosition"});
    }

    public void setColor(float r, float g, float b, float a) {
        mColor[0] = r;
        mColor[1] = g;
        mColor[2] = b;
        mColor[3] = a;
    }

    protected void reallocateBuffers() {
        mCapacity *= 2;

        // Reallocate vertex buffer
        float[] vertexData = new float[mCount * mVertexSize];
        mVertexBuffer.position(0);
        mVertexBuffer.get(vertexData, 0, mCount * mVertexSize);
        mVertexBuffer = null;

        mVertexBuffer = ByteBuffer
                .allocateDirect(mCapacity * mVertexSize * SIZEOF_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(vertexData);
        vertexData = null;

        // Reallocate drawlist buffer
        if (mDrawListSize > 0) {
            short[] drawListData = new short[mCount * mDrawListSize];
            mDrawListBuffer.position(0);
            mDrawListBuffer.get(drawListData, 0, mCount * mDrawListSize);
            mDrawListBuffer = null;
            mDrawListBuffer = ByteBuffer
                    .allocateDirect(mCapacity * mDrawListSize * SIZEOF_SHORT)
                    .order(ByteOrder.nativeOrder())
                    .asShortBuffer()
                    .put(drawListData);
            drawListData = null;
        }
    }

    protected abstract void drawElements();

    public void draw(float[] vpMatrix) {
        GLES20.glUseProgram(mProgram);

        // Retrieve handles to shader variables
        int positionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        int colorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        int mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // Pass in the position
        mVertexBuffer.position(0);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false, 0, mVertexBuffer);
        GLES20.glEnableVertexAttribArray(positionHandle);

        // Pass in the color for drawing
        GLES20.glUniform4fv(colorHandle, 1, mColor, 0);

        // Pass in the projection and view transformation
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, vpMatrix, 0);

        // Draw the rectangles
        if (mDrawListSize > 0)
            mDrawListBuffer.position(0);
        drawElements();

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    public int getCount() {
        return mCount;
    }

    public void clear() {
        mCount = 0;
        mVertexBuffer.clear();

        if (mDrawListSize > 0)
            mDrawListBuffer.clear();
    }

}