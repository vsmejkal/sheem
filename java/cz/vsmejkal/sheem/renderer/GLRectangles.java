package cz.vsmejkal.sheem.renderer;

import android.opengl.GLES20;


public class GLRectangles extends GLObjects {

    public GLRectangles() {
        super();
        mVertexSize = 4 * 2;    /* 4 vertices * 2 coors */
        mDrawListSize = 2 * 3;  /* 2 triangles * 3 indices */
        initialize();
    }

    public void addRect(float x, float y, float width, float height) {
        addPolygon(x, y, x + width, y, x + width, y + height, x, y + height);
    }

    public void addPolygon(float x1, float y1, float x2, float y2,
                           float x3, float y3, float x4, float y4) {
        // Buffers are full
        if (mCapacity == mCount)
            reallocateBuffers();

        final int idx = 4 * mCount;
        mCount++;

        mFloatBuffer[0] = x1;
        mFloatBuffer[1] = y1;
        mFloatBuffer[2] = x4;
        mFloatBuffer[3] = y4;
        mFloatBuffer[4] = x3;
        mFloatBuffer[5] = y3;
        mFloatBuffer[6] = x2;
        mFloatBuffer[7] = y2;
        mVertexBuffer.put(mFloatBuffer);

        mShortBuffer[0] = (short) (idx);
        mShortBuffer[1] = (short) (idx+1);
        mShortBuffer[2] = (short) (idx+2);
        mShortBuffer[3] = (short) (idx);
        mShortBuffer[4] = (short) (idx+2);
        mShortBuffer[5] = (short) (idx+3);
        mDrawListBuffer.put(mShortBuffer);
    }

    protected void drawElements() {
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, mDrawListSize * mCount,
                GLES20.GL_UNSIGNED_SHORT, mDrawListBuffer);
    }
}
