package cz.vsmejkal.sheem.renderer;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GLRenderer implements GLSurfaceView.Renderer {

    private static final String TAG = "GLRenderer";

    private int mWidth = 0;
    private int mHeight = 0;
    private Context mContext;
    private GLSurfaceView mParentView;

    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];

    // Declare as volatile because we are updating it from another thread
    public volatile int mScrollX = 0;
    public volatile int mScrollY = 0;

    private long time = 0;


    public GLRenderer(Context context, GLSurfaceView parentView) {
        mContext = context;
        mParentView = parentView;
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        // Set the background frame color
        GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

        // We don't need depth test in 2D mode
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);

        // Enable alpha blending
        GLES20.glEnable(GL10.GL_BLEND);
        GLES20.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);

        onInit(mContext);
    }

    public void onInit(Context ctx) {}

    @Override
    public void onDrawFrame(GL10 unused) {
        // Draw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0, 0, 0, 0, 1.0f, 0);

        // Translate view to scroll offset
        Matrix.translateM(mViewMatrix, 0, -mScrollX, -mScrollY, 0);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjMatrix, 0, mViewMatrix, 0);

        onDraw(mMVPMatrix);

//        long time2 = SystemClock.uptimeMillis();
//        Log.i("FPS:", Long.toString(1000 / (time2 - time)));
//        time = time2;
    }

    public void onDraw(float[] MVPMatrix) {};

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        mWidth = width;
        mHeight = height;

        // Adjust the viewport based on geometry changes, such as screen rotation
        GLES20.glViewport(0, 0, width, height);

        // Create orthographic projection
        Matrix.orthoM(mProjMatrix, 0, 0, -width, height, 0, .1f, 100f);
    }

    /**
     * Utility method for debugging OpenGL calls. Provide the name of the call
     * just after making it:
     *
     * <pre>
     * mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
     * GLRenderer.checkGlError("glGetUniformLocation");</pre>
     *
     * If the operation is not successful, the check throws an error.
     *
     * @param glOperation - Name of the OpenGL call to check.
     */
    public static void checkGlError(String glOperation) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, glOperation + ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }

    public void requestRender() {
        mParentView.requestRender();
    }
}


