package cz.vsmejkal.sheem.renderer;


import android.content.Context;
import cz.vsmejkal.sheem.R;

public class GLSymbols {

    public static final int TWO = 0;
    public static final int THREE = 1;
    public static final int FOUR = 2;
    public static final int SIX = 3;
    public static final int EIGHT = 4;
    public static final int NINE = 5;
    public static final int TWELVE = 6;
    public static final int ACCID_SHARP = 7;
    public static final int ACCID_FLAT = 8;
    public static final int ACCID_NATURAL = 9;
    public static final int ACCID_DOUBLESHARP = 10;
    public static final int RESTS_2 = 11;
    public static final int RESTS_3 = 12;
    public static final int FLAGS_U3 = 13;
    public static final int NOTEHEADS_S0 = 14;
    public static final int NOTEHEADS_S1 = 15;
    public static final int NOTEHEADS_S2 = 16;
    public static final int CLEFS_G = 17;
    public static final int CLEFS_F = 18;
    public static final int DOTS_DOT = 19;

    private static final float TEX_WIDTH = 256f;
    private static final float TEX_HEIGHT = 512f;

    private static final float[] mTiles = new float[] {
            // x1, y1, x2, y2
            2, 311, 54, 378,
            204, 90, 251, 157,
            201, 340, 254, 405,
            201, 270, 254, 338,
            2, 241, 57, 309,
            104, 267, 158, 334,
            2, 127, 97, 195,
            59, 302, 96, 422,
            98, 407, 129, 503,
            74, 197, 102, 300,
            204, 159, 246, 201,
            131, 383, 165, 496,
            204, 203, 244, 268,
            160, 267, 199, 381,
            2, 197, 72, 239,
            204, 46, 253, 88,
            204, 2, 253, 44,
            106, 2, 202, 265,
            2, 2, 104, 125,
            98, 383, 114, 399
    };

    private static float[] mDispl = null;
    private static int mTexture;
    private TextureAtlas mAtlas;


    public GLSymbols() {
        mAtlas = new TextureAtlas(mTexture);
    }

    public static void loadImages(Context ctx) {
        mTexture = GLHelper.loadTexture(ctx, R.drawable.notation_xhdpi);
    }

    public void add(float x1, float y1, float x2, float y2, int symbol) {
        final float tx1 = mTiles[4 * symbol] / TEX_WIDTH;
        final float ty1 = mTiles[4 * symbol + 1] / TEX_HEIGHT;
        final float tx2 = mTiles[4 * symbol + 2] / TEX_WIDTH;
        final float ty2 = mTiles[4 * symbol + 3] / TEX_HEIGHT;

        mAtlas.add(x1, y1, x2, y2, tx1, ty1, tx2, ty2);
    }

    public static float getWidth(int symbol) {
        return mTiles[4 * symbol + 2] - mTiles[4 * symbol];
    }

    public static float getHeight(int symbol) {
        return mTiles[4 * symbol + 3] - mTiles[4 * symbol + 1];
    }

    public static float aspectRatio(int symbol) {
        return getWidth(symbol) / getHeight(symbol);
    }

    public void draw(float[] vpMatrix) {
        mAtlas.draw(vpMatrix);
    }

    public void clear() {
        mAtlas.clear();
    }

    public int getCount() {
        return mAtlas.getCount();
    }
}
