package cz.vsmejkal.sheem.renderer;

import android.opengl.GLES20;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLImage {

    private final String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
            "attribute vec2 a_TexCoordinate;" +
            "varying vec2 v_TexCoordinate;" +
            "attribute vec4 vPosition;" +
            "void main() {" +
            "  gl_Position = uMVPMatrix * vPosition;" +
            "  v_TexCoordinate = a_TexCoordinate;" + "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
            "uniform sampler2D u_Texture;" +
            "varying vec2 v_TexCoordinate;" +
            "void main() {" +
            "  gl_FragColor = texture2D(u_Texture, v_TexCoordinate);" + "}";

    private FloatBuffer mVertexBuffer;
    private final ShortBuffer mDrawListBuffer;
    private FloatBuffer mTextureBuffer;

    private final int mProgram;
    private int mTexture;

    private float[] mTranslate = new float[16];
    private float[] mMVPMatrix = new float[16];
    private int[] mGeometry = new int[4];
    private float[] mTexCoors = new float[4];

    private final short drawOrder[] = { 0, 1, 2, 0, 2, 3 };

    public GLImage() {
        final int vertexLength = 4 * 3;
        final int texCoorLength = 4 * 2;

        // Initialize drawing buffers
        mDrawListBuffer = ByteBuffer
                .allocateDirect(drawOrder.length * 2)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer()
                .put(drawOrder);
        mDrawListBuffer.position(0);

        mVertexBuffer = ByteBuffer
                .allocateDirect(vertexLength * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mVertexBuffer.position(0);

        mTextureBuffer = ByteBuffer
                .allocateDirect(texCoorLength * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mTextureBuffer.position(0);

        setGeometry(0, 0, 0, 0);
        setTexCoors(0.5f, 0.5f, 1, 1);

        // prepare shaders and OpenGL program
        final int vertexShader = GLHelper.loadShader(
                GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        final int fragmentShader = GLHelper.loadShader(
                GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLHelper.createAndLinkProgram(vertexShader,
                fragmentShader, new String[]{"a_TexCoordinate", "vPosition"});
    }

    public void setGeometry(int x, int y, int width, int height)
    {
        setPosition(x, y);
        setSize(width, height);
    }

    public void setPosition(int x, int y)
    {
        mGeometry[0] = x;
        mGeometry[1] = y;
        Matrix.setIdentityM(mTranslate, 0);
        Matrix.translateM(mTranslate, 0, x, y, 0);
    }

    public void setSize(int width, int height)
    {
        mGeometry[2] = width;
        mGeometry[3] = height;

        // Set rectangle vertex coordinates
        mVertexBuffer.clear();
        mVertexBuffer.put(new float[] {
                0, 0, 0,
                0, height, 0,
                width, height, 0,
                width, 0, 0
        });
        mVertexBuffer.position(0);
    }

    public void setTexture(int texture) {
        mTexture = texture;
    }

    public void setTexCoors(float x1, float y1, float x2, float y2) {

        // Initialize texture coordinates
        mTextureBuffer.clear();
        mTextureBuffer.put(new float[] {
                x1, y1, // top left
                x1, y2, // bottom left
                x2, y2, // bottom right
                x2, y1  // top right
        });
        mTextureBuffer.position(0);
    }

    public void draw(float[] vpMatrix) {
        GLES20.glUseProgram(mProgram);

        // Retrieve handles to shader variables
        int textureHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        int positionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        int textureCoordHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
        int mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // Pass in the position information
//        mVertexBuffer.position(0);
        GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT,
                false, 0, mVertexBuffer);
        GLES20.glEnableVertexAttribArray(positionHandle);

        // Pass in the texture information
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture);
        GLES20.glUniform1i(textureHandle, 0);

        // Pass in the texture coordinate information
//        mTextureBuffer.position(0);
        GLES20.glVertexAttribPointer(textureCoordHandle, 2, GLES20.GL_FLOAT,
                false, 0, mTextureBuffer);
        GLES20.glEnableVertexAttribArray(textureCoordHandle);

        // Apply the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, vpMatrix, 0, mTranslate, 0);
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

        // Draw the rectangles
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
                GLES20.GL_UNSIGNED_SHORT, mDrawListBuffer);

        // Disable arrays
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(textureCoordHandle);
    }

}