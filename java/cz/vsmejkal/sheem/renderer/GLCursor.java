package cz.vsmejkal.sheem.renderer;

import android.opengl.GLES20;


public class GLCursor extends GLObjects {

    public float mWidth = 12;
    public boolean mIsVisible = true;

    public GLCursor() {
        super();
        mVertexSize = 4 * 2;    /* 4 vertices * 2 coors */
        mDrawListSize = 2 * 3;  /* 2 triangles * 3 indices */
        mCapacity = 1;
        mCount = 1;

        initialize();
        setColor(0.75f, 0.89f, 1.0f, 1.0f);

        mShortBuffer[0] = 0;
        mShortBuffer[1] = 1;
        mShortBuffer[2] = 2;
        mShortBuffer[3] = 0;
        mShortBuffer[4] = 2;
        mShortBuffer[5] = 3;
        mDrawListBuffer.position(0);
        mDrawListBuffer.put(mShortBuffer);
    }

    public void setPosition(float x, float y, float height) {
        final float width2 = mWidth / 2;

        mFloatBuffer[0] = x - width2;
        mFloatBuffer[1] = y;
        mFloatBuffer[2] = x - width2;
        mFloatBuffer[3] = y + height;
        mFloatBuffer[4] = x + width2;
        mFloatBuffer[5] = y + height;
        mFloatBuffer[6] = x + width2;
        mFloatBuffer[7] = y;
        mVertexBuffer.position(0);
        mVertexBuffer.put(mFloatBuffer);
    }

    protected void drawElements() {
        if (mIsVisible) {
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, mDrawListSize * mCount,
                    GLES20.GL_UNSIGNED_SHORT, mDrawListBuffer);
        }
    }

    public void setVisibility(boolean visible) {
        mIsVisible = visible;
    }
}
