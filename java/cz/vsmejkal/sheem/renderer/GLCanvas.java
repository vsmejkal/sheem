package cz.vsmejkal.sheem.renderer;

import android.graphics.Paint;
import android.opengl.Matrix;


public class GLCanvas {
    private GLLines         mLines;
    private GLRectangles    mRects;
    private GLSymbols       mSymbols;
    private GLCursor        mCursor;

    private int     mWidth;
    private int     mHeight;
    private float   offsetX = 0;
    private float   offsetY = 0;
    private float   mZoom = 1;

    public GLCanvas(int width, int height) {
        mWidth = width;
        mHeight = height;
        mLines = new GLLines();
        mSymbols = new GLSymbols();
        mRects = new GLRectangles();
        mCursor = new GLCursor();
    }

    public void clear() {
        mLines.clear();
        mSymbols.clear();
        mRects.clear();
    }

    public void translate(float x, float y) {
        offsetX += x;
        offsetY += y;
    }

    public void setOffset(float x, float y) {
        offsetX = x;
        offsetY = y;
    }

    public void setSize(int w, int h) {
        mWidth = w;
        mHeight = h;
    }

    public void setZoom(float zoom) {
        mZoom = zoom;
        mLines.setLineWidth((zoom > 1.0f) ? zoom : 1);
    }

    public void drawLine(float x1, float y1, float x2, float y2, Paint paint) {
        float stroke = paint.getStrokeWidth();

        if (stroke <= 1.5) {
            mLines.add(offsetX + x1, offsetY + y1, offsetX + x2, offsetY + y2);
        }
        else {
            mRects.addPolygon(
                    x1 + offsetX, y1 + offsetY - .5f * stroke,
                    x2 + offsetX, y2 + offsetY - .5f * stroke,
                    x2 + offsetX, y2 + offsetY + .5f * stroke,
                    x1 + offsetX, y1 + offsetY + .5f * stroke);
        }
    }

    public void drawRect(float left, float top, float right, float bottom, Paint paint) {
        mRects.addRect(offsetX + left, offsetY + top, right - left, bottom - top);
    }

    public void drawText(final String text, float x, float y, Paint paint) {

    }

    public void drawSymbol(float x1, float y1, float x2, float y2, int symbol) {
        mSymbols.add(x1 + offsetX, y1 + offsetY, x2 + offsetX, y2 + offsetY, symbol);
    }

    public void setCursor(float x1, float y1, float height) {
        mCursor.setPosition(x1 + offsetX, y1 + offsetY, height);
    }

    public void draw(float[] vpMatrix) {
        Matrix.scaleM(vpMatrix, 0, mZoom, mZoom, 1);

        mCursor.draw(vpMatrix);
        mLines.draw(vpMatrix);
        mRects.draw(vpMatrix);
        mSymbols.draw(vpMatrix);
    }

    public int getWidth() {
        return mWidth;
    }
    public int getHeight() {
        return mHeight;
    }
    public GLCursor getCursor() {
        return mCursor;
    }
}
