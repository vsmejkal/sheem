package cz.vsmejkal.sheem.renderer;

import android.opengl.GLES20;


public class GLLines extends GLObjects {

    public float mLineWidth = 1;

    public GLLines() {
        mVertexSize = 2 * 2;    /* 2 vertices * 2 coors */
        mDrawListSize = 0;      /* Don't use drawlist for lines */
        mCapacity = 128;
        initialize();
    }

    public void setLineWidth(float lineWidth) {
        mLineWidth = lineWidth;
    }

    public void add(float x1, float y1, float x2, float y2) {
        // Buffers are full
        if (mCapacity == mCount)
            reallocateBuffers();

        mCount++;

        mFloatBuffer[0] = x1;
        mFloatBuffer[1] = y1;
        mFloatBuffer[2] = x2;
        mFloatBuffer[3] = y2;
        mVertexBuffer.put(mFloatBuffer);
    }

    protected void drawElements() {
        GLES20.glLineWidth(mLineWidth);
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, 2 * mCount);
    }
}
