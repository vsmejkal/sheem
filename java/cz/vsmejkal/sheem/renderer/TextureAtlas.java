package cz.vsmejkal.sheem.renderer;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class TextureAtlas {

    private final String vertexShaderCode =
                    "uniform mat4 uMVPMatrix;" +
                    "attribute vec2 a_TexCoordinate;" +
                    "varying vec2 v_TexCoordinate;" +
                    "attribute vec3 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vec4(vPosition, 1.0);" +
                    "  v_TexCoordinate = a_TexCoordinate;" + "}";

    private final String fragmentShaderCode =
                    "precision mediump float;" +
                    "uniform sampler2D u_Texture;" +
                    "varying vec2 v_TexCoordinate;" +
                    "void main() {" +
                    "  gl_FragColor = texture2D(u_Texture, v_TexCoordinate);" + "}";

    protected FloatBuffer mVertexBuffer;
    protected ShortBuffer mDrawListBuffer;
    protected FloatBuffer mTexCoorBuffer;

    static final int SIZEOF_FLOAT = 4;
    static final int SIZEOF_SHORT = 2;

    /* 4 vertices * 2 coors */
    private static int mVertexSize = 4 * 2;
    /* 2 triangles * 3 indices */
    private static int mDrawListSize = 2 * 3;
    /* 4 points * 2 coors */
    private static int mTexCoorSize = 4 * 2;

    // Helper buffers used for filling main buffers
    private float[] mFloatBuffer;
    private short[] mShortBuffer;

    private int mCount = 0;
    private int mCapacity = 128;
    private int mTexture;
    private int mProgram;


    public TextureAtlas(int texture) {
        mTexture = texture;
        mFloatBuffer = new float[mVertexSize];
        mShortBuffer = new short[mDrawListSize];
        initialize();
    }

    private void initialize() {
        mVertexBuffer = ByteBuffer
                .allocateDirect(mCapacity * mVertexSize * SIZEOF_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();

        mDrawListBuffer = ByteBuffer
                .allocateDirect(mCapacity * mDrawListSize * SIZEOF_SHORT)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer();

        mTexCoorBuffer = ByteBuffer
                .allocateDirect(mCapacity *  mTexCoorSize * SIZEOF_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();

        // Prepare shaders and OpenGL program
        final int vertexShader = GLHelper.loadShader(
                GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        final int fragmentShader = GLHelper.loadShader(
                GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLHelper.createAndLinkProgram(vertexShader,
                fragmentShader, new String[]{"a_TexCoordinate", "vPosition"});
    }

    private void reallocateBuffers() {
        mCapacity *= 2;

        // Reallocate vertex buffer
        float[] vertexData = new float[mCount * mVertexSize];
        mVertexBuffer.position(0);
        mVertexBuffer.get(vertexData, 0, mCount * mVertexSize);
        mVertexBuffer = null;

        mVertexBuffer = ByteBuffer
                .allocateDirect(mCapacity * mVertexSize * SIZEOF_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(vertexData);
        vertexData = null;

        // Reallocate drawlist buffer
        short[] drawListData = new short[mCount * mDrawListSize];
        mDrawListBuffer.position(0);
        mDrawListBuffer.get(drawListData, 0, mCount * mDrawListSize);
        mDrawListBuffer = null;
        mDrawListBuffer = ByteBuffer
                .allocateDirect(mCapacity * mDrawListSize * SIZEOF_SHORT)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer()
                .put(drawListData);
        drawListData = null;

        float[] texCoorData = new float[mCount * mTexCoorSize];
        mTexCoorBuffer.position(0);
        mTexCoorBuffer.get(texCoorData, 0, mCount * mTexCoorSize);
        mTexCoorBuffer = null;
        mTexCoorBuffer = ByteBuffer
                .allocateDirect(mCapacity * mTexCoorSize * SIZEOF_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(texCoorData);
        texCoorData = null;

    }

    public void add(float x1, float y1, float x2, float y2,
                    float tx1, float ty1, float tx2, float ty2) {
        // Buffers are full
        if (mCapacity == mCount)
            reallocateBuffers();

        final int idx = 4 * mCount;
        mCount++;

        mFloatBuffer[0] = x1;
        mFloatBuffer[1] = y1;
        mFloatBuffer[2] = x1;
        mFloatBuffer[3] = y2;
        mFloatBuffer[4] = x2;
        mFloatBuffer[5] = y2;
        mFloatBuffer[6] = x2;
        mFloatBuffer[7] = y1;
        mVertexBuffer.put(mFloatBuffer);

        mShortBuffer[0] = (short) (idx);
        mShortBuffer[1] = (short) (idx+1);
        mShortBuffer[2] = (short) (idx+2);
        mShortBuffer[3] = (short) (idx);
        mShortBuffer[4] = (short) (idx+2);
        mShortBuffer[5] = (short) (idx+3);
        mDrawListBuffer.put(mShortBuffer);

        mFloatBuffer[0] = tx1;
        mFloatBuffer[1] = ty1;
        mFloatBuffer[2] = tx1;
        mFloatBuffer[3] = ty2;
        mFloatBuffer[4] = tx2;
        mFloatBuffer[5] = ty2;
        mFloatBuffer[6] = tx2;
        mFloatBuffer[7] = ty1;
        mTexCoorBuffer.put(mFloatBuffer);
    }

    public void draw(float[] vpMatrix) {
        GLES20.glUseProgram(mProgram);

        // Retrieve handles to shader variables
        int textureHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        int positionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        int textureCoorHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
        int mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // Pass in the position information
        mVertexBuffer.position(0);
        GLES20.glVertexAttribPointer(positionHandle, 2, GLES20.GL_FLOAT,
                false, 0, mVertexBuffer);
        GLES20.glEnableVertexAttribArray(positionHandle);

        // Pass in the texture information
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture);
        GLES20.glUniform1i(textureHandle, 0);

        // Pass in the texture coordinate information
        mTexCoorBuffer.position(0);
        GLES20.glVertexAttribPointer(textureCoorHandle, 2, GLES20.GL_FLOAT,
                false, 0, mTexCoorBuffer);
        GLES20.glEnableVertexAttribArray(textureCoorHandle);

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, vpMatrix, 0);

        // Draw the rectangles
        mDrawListBuffer.position(0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, mDrawListSize * mCount,
                GLES20.GL_UNSIGNED_SHORT, mDrawListBuffer);

        // Disable arrays
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(textureCoorHandle);
    }

    public void clear() {
        mCount = 0;
        mVertexBuffer.clear();
        mTexCoorBuffer.clear();
        mDrawListBuffer.clear();
    }

    public int getCount() {
        return mCount;
    }

}