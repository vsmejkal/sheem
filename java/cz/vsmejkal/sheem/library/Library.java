package cz.vsmejkal.sheem.library;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by vojta on 23.7.14.
 */
public class Library {
    // Filename of library database
    private static final String LIBRARY_FILE = "library.json";

    private ScoreArray mScores;


    public void load(Context ctx) throws IOException {
        String path = ctx.getFilesDir().getAbsolutePath() + "/" + LIBRARY_FILE;
        File file = new File(path);

        if (!file.exists())
            file.createNewFile();

        FileInputStream istream = ctx.openFileInput(LIBRARY_FILE);
        byte[] buffer = new byte[istream.available()];
        istream.read(buffer);
        istream.close();

        String jsonData = new String(buffer, "UTF-8");
        mScores = new ScoreArray(jsonData);
    }

    public boolean store(ScoreInfo score, Context ctx) {
        // Check if file hasn't been already downloaded
        for (ScoreInfo item: mScores)
            if (item.download_uri.equals(score.download_uri))
                return false;

        // Download file
        String path = ctx.getFilesDir().getAbsolutePath();
        String fileName = Integer.toString(score.download_uri.hashCode());
        downloadFile(score.download_uri, path + "/" + fileName);

        mScores.add(score);
        return true;
    }

    public void delete(ScoreInfo score, Context ctx) {
        String path = ctx.getFilesDir().getAbsolutePath() + File.separator + score.filename;
        File file = new File(path);

        if (file.exists())
            file.delete();

        mScores.remove(score);
    }

    public ScoreArray search(final String text) {
        ScoreArray result = new ScoreArray();
        for (ScoreInfo score: mScores)
            if (score.match(text))
                result.add(score);

        return result;
    }

    private void downloadFile(final String urlLink, final String filePath) {
        Thread dx = new Thread() {
            public void run() {
                try {
                    URL url = new URL(urlLink);
                    Log.i("FILE_NAME", "File name is " + filePath);
                    Log.i("FILE_URLLINK", "File URL is " + url);
                    URLConnection connection = url.openConnection();
                    connection.connect();

                    // this will be useful so that you can show a typical 0-100% progress bar
                    int fileLength = connection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream());
                    OutputStream output = new FileOutputStream(filePath);

                    byte data[] = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("ERROR ON DOWNLOADING FILES", "ERROR IS" + e);
                }
            }
        };
        dx.start();
    }
}
