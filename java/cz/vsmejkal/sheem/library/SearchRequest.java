package cz.vsmejkal.sheem.library;

/**
 * Created by vojta on 23.7.14.
 */
public class SearchRequest {
    // Default count of items per one search
    public static final int RESULTS_PER_PAGE = 20;

    // Search phrase
    public String text;

    // MIDI number of musical instrument
    public int instrument = 0;

    // Page number (typically 20 results per page)
    public int page = 0;

    // Search for scores with one or multiple parts
    public boolean solo = true;

    public SearchRequest(final String query) {
        text = query;
    }
}
