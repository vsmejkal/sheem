package cz.vsmejkal.sheem.library;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by vojta on 23.7.14.
 */
public class ScoreArray implements Iterable<ScoreInfo> {
    private ArrayList<ScoreInfo> mScores = new ArrayList<ScoreInfo>();

    public ScoreArray() {
        mScores = new ArrayList<ScoreInfo>();
    }

    public ScoreArray(final String json) {
        try {
            JSONArray arr = new JSONArray(json);
            mScores = new ArrayList<ScoreInfo>(arr.length());

            for (int i = 0; i < arr.length(); i++) {
                ScoreInfo score = new ScoreInfo(arr.getJSONObject(i));
                mScores.add(score);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Iterator<ScoreInfo> iterator() {
        return mScores.iterator();
    }

    public void add(ScoreInfo score) {
        mScores.add(score);
    }

    public void remove(ScoreInfo score) {
        mScores.remove(score);
    }

    public int length() {
        return mScores.size();
    }

    public void addAll(ScoreArray array) {
        mScores.addAll(array.getArray());
    }

    public void replaceAll(ScoreArray array) {
        mScores.clear();
        mScores.addAll(array.getArray());
    }

    public ArrayList<ScoreInfo> getArray() {
        return mScores;
    }

    public String toString() {
        String output = "";
        for (ScoreInfo score: mScores)
            output += score.toString();
        return output;
    }
}
