package cz.vsmejkal.sheem.library;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vojta on 23.7.14.
 */
public class ScoreInfo {
    // Full title of the score
    public String title = "";

    // Name of the composer or music band
    public String composer = "";

    // Duration in seconds
    public int duration = 0;

    // Name of file in local storage
    public String filename = "";

    // Link to the score page
    public String link = "";

    // Path to the online resource
    public String download_uri = "";

    // Instrument MIDI programs of score parts
    public int[] parts = new int[]{};

    // Names of score parts
    public String[] parts_names = new String[]{};


    public ScoreInfo() {}

    public ScoreInfo(JSONObject item) throws JSONException {
        title = item.getString("title");
        composer = item.getString("composer");
        duration = item.getInt("duration");
        filename = item.getString("filename");
        link = item.getString("link");
        download_uri = item.getString("download_uri");

        JSONArray partsJSON = item.getJSONArray("parts");
        parts = new int[partsJSON.length()];
        for (int i = 0; i < partsJSON.length(); i++)
            parts[i] = partsJSON.getInt(i);

        JSONArray partsNamesJSON = item.getJSONArray("parts_names");
        parts_names = new String[partsNamesJSON.length()];
        for (int i = 0; i < partsNamesJSON.length(); i++)
            parts_names[i] = partsNamesJSON.getString(i);
    }

    public boolean match(String text) {
        text = text.toLowerCase();
        return title.contains(text) || composer.contains(text);
    }

    public String toString() {
        JSONObject item = new JSONObject();
        JSONArray partsJSON = new JSONArray();
        JSONArray partsNamesJSON = new JSONArray();

        for (int p: parts)
            partsJSON.put(p);
        for (String pn: parts_names)
            partsNamesJSON.put(pn);
        try {
            item.put("title", title);
            item.put("composer", composer);
            item.put("duration", duration);
            item.put("filename", filename);
            item.put("link", link);
            item.put("download_uri", download_uri);
            item.put("parts", partsJSON);
            item.put("parts_names", partsNamesJSON);
        } catch (JSONException e) {
            e.printStackTrace();
            return "{}";
        }
        return item.toString();
    }
}
