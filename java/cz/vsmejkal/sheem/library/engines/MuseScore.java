package cz.vsmejkal.sheem.library.engines;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;

import cz.vsmejkal.sheem.R;
import cz.vsmejkal.sheem.library.ScoreArray;
import cz.vsmejkal.sheem.library.ScoreInfo;
import cz.vsmejkal.sheem.library.SearchEngine;
import cz.vsmejkal.sheem.library.SearchRequest;
import cz.vsmejkal.sheem.midi.MidiFile;

/**
 * Created by vojta on 23.7.14.
 */
public class MuseScore implements SearchEngine {

    // URL base for downloading scores
    private static final String DOWNLOAD_URL = "http://static.musescore.com/";

    // MuseScore API Endpoint
    private static final String API_URL = "http://api.musescore.com/services/rest/";

    // Private consumer key
    private static final String CONSUMER_KEY = "xxx";

    private Thread mThread;
    private Listener mListener;

    public MuseScore(Listener listener) {
        setListener(listener);
    }

    @Override
    public void search(final SearchRequest request) {
        mThread = new Thread() {
            @Override
            public void run() {
                try {
                    String content = download(request);
                    ScoreArray results = parseResults(content);
                    mListener.onSearchFinished(results);
                } catch (UnknownHostException e) {
                    mListener.onSearchError(R.string.connection_err);
                } catch (ConnectException e) {
                    mListener.onSearchError(R.string.connection_err);
                } catch (IOException e) {
                    e.printStackTrace();
                    mListener.onSearchError(R.string.server_err);
                } catch (JSONException e) {
                    e.printStackTrace();
                    mListener.onSearchError(R.string.server_err);
                }
            }
        };
        mThread.start();
    }

    @Override
    public void stop() {
        if (mThread != null && !mThread.isInterrupted()) {
            mThread.interrupt();
            try {
                mThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mThread = null;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    private String download(SearchRequest req) throws IOException {
        URL url = new URL(requestURL(req));
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

        String inputLine, content = "";
        while ((inputLine = reader.readLine()) != null)
            content += inputLine;
        reader.close();

        return content;
    }

    private String requestURL(SearchRequest request) {
        String query = "";
        try {
            query = URLEncoder.encode(request.text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return API_URL + "score.json"
                + "&oauth_consumer_key=" + CONSUMER_KEY
                + "&text=" + query
                + "&page=" + request.page
                + "&part=" + request.instrument
                + "&parts=" + (request.solo ? "1" : "0");
    }

    private ScoreArray parseResults(final String json) throws JSONException {
        ScoreArray scores = new ScoreArray();
        JSONArray items = new JSONArray(json);

        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);
            JSONObject metadata = item.getJSONObject("metadata");
            ScoreInfo score = new ScoreInfo();

            score.title = metadata.getString("title");
            score.composer = metadata.getString("composer");
            score.duration = metadata.getInt("duration");
            score.link = item.getString("permalink");
            score.download_uri = parseDownloadURI(item);
            score.parts = parseParts(metadata);
            score.parts_names = parsePartsNames(metadata);

            if (score.title.equals("null"))
                score.title = item.getString("title");

            if (score.composer.equals("null"))
                score.composer = "";

            scores.add(score);
        }

        return scores;
    }

    private int[] parseParts(JSONObject metadata) throws JSONException {
        JSONArray arr = metadata.getJSONArray("parts");
        int[] parts = new int[arr.length()];

        for (int j = 0; j < arr.length(); j++)
            parts[j] = arr.getJSONObject(j).getJSONObject("part").getInt("program");

        return parts;
    }

    private String[] parsePartsNames(JSONObject metadata) throws JSONException {
        JSONArray arr = metadata.getJSONArray("parts");
        String[] partsNames = new String[arr.length()];

        for (int j = 0; j < arr.length(); j++) {
            partsNames[j] = arr.getJSONObject(j).getJSONObject("part").getString("name");

            if (partsNames[j].equals("null")) {
                int program = arr.getJSONObject(j).getJSONObject("part").getInt("program");
                partsNames[j] = MidiFile.Instruments[program];
            }
        }

        return partsNames;
    }

    private String parseDownloadURI(JSONObject item) throws JSONException {
        return DOWNLOAD_URL
                + item.getString("id") + "/"
                + item.getString("secret") + "/"
                + "score.mid";
    }
}
