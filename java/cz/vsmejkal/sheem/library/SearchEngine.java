package cz.vsmejkal.sheem.library;

/**
 * Created by vojta on 23.7.14.
 */
public interface SearchEngine {
    // Perform search for given request
    public void search(SearchRequest request);

    // Interrupt the searching thread
    public void stop();

    // Set search engine listener
    public void setListener(Listener listener);

    public interface Listener {
        // Publish search results
        public void onSearchFinished(ScoreArray results);

        // When some error occurs
        public void onSearchError(int err);
    }
}

